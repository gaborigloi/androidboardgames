package uk.ac.cam.gi225.boardgames;

import java.util.Random;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
//import android.graphics.Paint.Align;
//import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
//import android.graphics.Typeface;
import android.view.MotionEvent;

/**
 * Tic-tac-toe / Xs and Os
 * @author gabor
 *
 */
public class aGame extends SG {
	
	private static final int X = -1;
	private static final int O = 1;
	private static final int EMPTY = 0;
	private static final int NONE = 0;
	private static final int DRAW = 2;
	private static final int PAINT_LINE = 0;
	private static final int PAINT_X = 1;
	private static final int PAINT_O = 2;
	private static final int PAINT_LINE2 = 3;
	private int round,winner,gsize,me;
	private int[][] table = new int[3][3];
	private int[] border;
	private Paint[] gp;

	public aGame(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		//gameMode = SelectGame.MULTIPLAYER_PHONE;
		gp = new Paint[4];
		for (int i=0; i<gp.length; i++) {
			gp[i] = new Paint();
			gp[i].setAntiAlias(true);
		}
		gp[PAINT_LINE].setColor(Color.BLACK);
		gp[PAINT_LINE].setStrokeWidth(6);
		gp[PAINT_X].setColor(Color.BLUE);
		gp[PAINT_X].setStrokeWidth(10);
		gp[PAINT_O].setColor(Color.RED);
		gp[PAINT_O].setStrokeWidth(10);
		gp[PAINT_O].setStyle(Style.STROKE);
		gp[PAINT_LINE2].setColor(Color.RED);
		gp[PAINT_LINE2].setStrokeWidth(6);
		//gp[PAINT_LINE].setTypeface(Typeface.SANS_SERIF);
		//gp[PAINT_LINE].setStyle(Style.STROKE);
		//gp[PAINT_LINE].setStrokeCap(Cap.ROUND);
		//gp[PAINT_LINE].setStrokeJoin(Join.ROUND);
		//gp[PAINT_LINE].setAntiAlias(true);
		//gp[PAINT_LINE].setDither(true);
		//gp[PAINT_LINE].setShadowLayer(10, 10, 6, Color.DKGRAY);
		border = new int[] { 0,0,0,0};
		for (int i=0; i<3; i++)
			for (int j=0; j<3; j++)
				table[i][j] = EMPTY;
		round = X;
		winner = NONE;		
	}
	
	@Override
	public void setGameMode(int gm) {
		gameMode = gm;
		if (gameMode==SelectGame.SINGLEPLAYER) {
			Random r = new Random();
			me = r.nextBoolean() ? X : O;
			if (round!=me) {
				AIMove();
				round*=-1;
				invalidate();
			}
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if ((winner==NONE)&&(event.getX()>=border[0])&&(event.getX()<=border[2])&&(event.getY()>=border[1])&&(event.getY()<=border[3])) {
			int sx = 0;
			while (event.getX()>border[0]+sx*gsize/3)
				sx++;
			int sy = 0;
			while (event.getY()>border[1]+sy*gsize/3)
				sy++;
			if ((sx<4)&&(sx>0)&&(sy<4)&&(sy>0)&&(((gameMode==SelectGame.SINGLEPLAYER)&&(round==me))||(gameMode==SelectGame.MULTIPLAYER_PHONE)))
				if (table[sx-1][sy-1]==EMPTY) {
					table[sx-1][sy-1] = round;
					if (checkWin())
						winner = round;
					else if (isDraw())
						winner = DRAW;
					round*=-1;
					if ((gameMode==SelectGame.SINGLEPLAYER)&&(winner==NONE)) {
						AIMove();
						if(checkWin())
							winner = round;
						else if (isDraw())
							winner = DRAW;
						round*=-1;
					}
					invalidate();
				}
		}
		return false;
	}
	
	private boolean isDraw() {
		boolean id  = true;
		for (int i=0; i<3; i++)
			for (int j=0; j<3; j++)
				if (table[i][j]!=EMPTY)
					id = false;
		return id;
	}
	
	private boolean checkWin() {
		if ((table[0][0]==table[0][1])&&(table[0][0]==table[0][2])&&(table[0][0]==round)) 
			return true;
		if ((table[1][0]==table[1][1])&&(table[1][0]==table[1][2])&&(table[1][0]==round)) 
			return true;
		if ((table[2][0]==table[2][1])&&(table[2][0]==table[2][2])&&(table[2][0]==round)) 
			return true;		
		if ((table[0][0]==table[1][0])&&(table[0][0]==table[2][0])&&(table[0][0]==round)) 
			return true;
		if ((table[0][1]==table[1][1])&&(table[0][1]==table[2][1])&&(table[0][1]==round)) 
			return true;
		if ((table[0][2]==table[1][2])&&(table[0][2]==table[2][2])&&(table[0][2]==round)) 
			return true;
		if ((table[0][0]==table[1][1])&&(table[0][0]==table[2][2])&&(table[0][0]==round))
			return true;
		if ((table[0][2]==table[1][1])&&(table[0][2]==table[2][0])&&(table[0][2]==round))
			return true;
		return false;
	}
	
	private float[] getcoords(int win) {
		if ((table[0][0]==table[0][1])&&(table[0][0]==table[0][2])&&(table[0][0]==win)) 
			return new float[] { border[0]+gsize/6,border[1]+gsize/6,border[0]+gsize/6,border[3]-gsize/6 };
		if ((table[1][0]==table[1][1])&&(table[1][0]==table[1][2])&&(table[1][0]==win)) 
			return new float[] { border[0]+gsize/3+gsize/6,border[1]+gsize/6,border[0]+gsize/3+gsize/6,border[3]-gsize/6 };
		if ((table[2][0]==table[2][1])&&(table[2][0]==table[2][2])&&(table[2][0]==win)) 
			return new float[] { border[2]-gsize/6,border[1]+gsize/6,border[2]-gsize/6,border[3]-gsize/6 };		
		if ((table[0][0]==table[1][0])&&(table[0][0]==table[2][0])&&(table[0][0]==win)) 
			return new float[] { border[0]+gsize/6,border[1]+gsize/6,border[2]-gsize/6,border[1]+gsize/6 };
		if ((table[0][1]==table[1][1])&&(table[0][1]==table[2][1])&&(table[0][1]==win)) 
			return new float[] { border[0]+gsize/6,border[1]+gsize/3+gsize/6,border[2]-gsize/6,border[1]+gsize/3+gsize/6 };
		if ((table[0][2]==table[1][2])&&(table[0][2]==table[2][2])&&(table[0][2]==win)) 
			return new float[] { border[0]+gsize/6,border[3]-gsize/6,border[2]-gsize/6,border[3]-gsize/6 };
		if ((table[0][0]==table[1][1])&&(table[0][0]==table[2][2])&&(table[0][0]==win))
			return new float[] { border[0]+gsize/6, border[1]+gsize/6, border[2]-gsize/6, border[3]-gsize/6 };
		if ((table[0][2]==table[1][1])&&(table[0][2]==table[2][0])&&(table[0][2]==win))
			return new float[] { border[0]+gsize/6, border[3]-gsize/6, border[2]-gsize/6, border[1]+gsize/6 };
		return new float[] {};
	}
	
	private boolean AIMove() {
		if (AIWin())
			return true;
		if (AIBlock())
			return true;
		if (AIFork())
			return true;
		if (AIBlockOpForkOpt2())
			return true;
		if (AICenter())
			return true;
		if (AIOppositeCorner())
			return true;
		if (AIEmptyCorner())
			return true;
		if (AIEmptySide())
			return true;
		return false;
	}
	
	private boolean AIWin() {
		for (int i=0; i<3; i++)
			for (int j=0; j<3; j++)
				if (table[i][j]==EMPTY) {
					table[i][j]=round;
					if (checkWin())
						return true;
					table[i][j] = EMPTY;
				}
		return false;
	}
	
	private boolean AIBlock() {
		round*=-1;
		for (int i=0; i<3; i++)
			for (int j=0; j<3; j++)
				if (table[i][j]==EMPTY) {
					table[i][j]=round;
					if (checkWin()) {
						round*=-1;
						table[i][j]=round;
						return true;
					}
					table[i][j]=EMPTY;
				}
		round*=-1;
		return false;
	}
	
	private boolean AIFork() {
		int winNum = 0;
		for (int i=0; i<3; i++)
			for (int j=0; j<3; j++)
				if (table[i][j]==EMPTY) {
					table[i][j]=round;
					winNum=0;
					for (int k=0; k<3; k++)
						for (int l=0; l<3; l++)
							if (table[k][l]==EMPTY) {
								table[k][l]=round;
								if (checkWin())
									winNum++;
								table[k][l]=EMPTY;
							}
					if (winNum>=2) {
						table[i][j]=round;
						return true;
					}
					table[i][j] = EMPTY;
				}
		return false;
	}
	
	private boolean AIBlockOpForkOpt2() {
		int winNum = 0;
		round*=-1;
		for (int i=0; i<3; i++)
			for (int j=0; j<3; j++)
				if (table[i][j]==EMPTY) {
					table[i][j]=round;
					winNum=0;
					for (int k=0; k<3; k++)
						for (int l=0; l<3; l++)
							if (table[k][l]==EMPTY) {
								table[k][l]=round;
								if (checkWin())
									winNum++;
								table[k][l]=EMPTY;
							}
					if (winNum>=2) {
						round*=-1;
						table[i][j]=round;
						return true;
					}
					table[i][j] = EMPTY;
				}
		round*=-1;
		return false;
	}
	
	private boolean AICenter() {
		boolean firstMove = true;
		for (int i=0; i<3; i++)
			for (int j=0; j<3; j++)
				if (table[i][j]!=EMPTY) {
					firstMove = false;
					break;
				}
		if (firstMove) {
			table[0][0]=round;
			return true;
		}
		if (table[1][1]==EMPTY) {
			table[1][1]=round;
			return true;
		}
		return false;
	}
	
	private boolean AIOppositeCorner() {
		if ((table[0][0]==round*-1)&&(table[2][2]==EMPTY)) {
			table[2][2]=round;
			return true;
		}
		if ((table[2][2]==round*-1)&&(table[0][0]==EMPTY)) {
			table[0][0]=round;
			return true;
		}
		if ((table[0][2]==round*-1)&&(table[2][0]==EMPTY)) {
			table[2][0]=round;
			return true;
		}
		if ((table[2][0]==round*-1)&&(table[0][2]==EMPTY)) {
			table[0][2]=round;
			return true;
		}
		return false;
	}
	
	private boolean AIEmptyCorner() {
		if (table[0][0]==EMPTY) {
			table[0][0]=round;
			return true;
		}
		if (table[2][2]==EMPTY) {
			table[2][2]=round;
			return true;
		}
		if (table[0][2]==EMPTY) {
			table[0][2]=round;
			return true;
		}
		if (table[2][0]==EMPTY) {
			table[2][0]=round;
			return true;
		}
		return false;
	}
	
	private boolean AIEmptySide() {
		if (table[0][1]==EMPTY) {
			table[0][1]=round;
			return true;
		}
		if (table[2][1]==EMPTY) {
			table[2][1]=round;
			return true;
		}
		if (table[1][0]==EMPTY) {
			table[1][0]=round;
			return true;
		}
		if (table[1][2]==EMPTY) {
			table[1][2]=round;
			return true;
		}
		return false;
	}
	
	@Override
	public void onDraw(Canvas canvas) {
		gsize=canvas.getWidth()<canvas.getHeight()?canvas.getWidth():canvas.getHeight();
		border = new int[] {canvas.getWidth()/2 - gsize/2,canvas.getHeight()/2 - gsize/2,
				canvas.getWidth()/2 + gsize/2,canvas.getHeight()/2 + gsize/2};
		float[] pts = new float[] { border[0]+4,border[1]+gsize/3, border[2]-4, border[1]+gsize/3,
									border[0]+4,border[1]+gsize*2/3, border[2]-4, border[1]+gsize*2/3,
									border[0]+gsize/3,border[1]+4, border[0]+gsize/3, border[3]-4,
									border[0]+gsize*2/3,border[1]+4, border[0]+gsize*2/3, border[3]-4,};
		gp[PAINT_X].setTextSize(gsize*6/15);
		canvas.drawLines(pts, gp[PAINT_LINE]);		
		for (int i=0; i<3; i++)
			for (int j=0; j<3; j++)
				switch(table[i][j]) {
				case X:
					canvas.drawLine(border[0]+gsize*i/3+gsize*2/30, border[1]+gsize*j/3+gsize*2/30, border[0]+gsize*(i+1)/3-gsize*2/30, border[1]+gsize*(j+1)/3-gsize*2/30, gp[PAINT_X]);
					canvas.drawLine(border[0]+gsize*i/3+gsize*2/30, border[1]+gsize*(j+1)/3-gsize*2/30, border[0]+gsize*(i+1)/3-gsize*2/30, border[1]+gsize*j/3+gsize*2/30, gp[PAINT_X]);
					break;
				case O:
					canvas.drawCircle(border[0]+gsize/6+gsize*i/3, border[1]+gsize/6+gsize*j/3, gsize/10, gp[PAINT_O]);
					break;
				}
		if ((winner!=NONE)&&(winner!=DRAW))
			canvas.drawLines(getcoords(winner), gp[PAINT_LINE2]);
	}

}
