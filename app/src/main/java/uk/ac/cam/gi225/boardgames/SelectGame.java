package uk.ac.cam.gi225.boardgames;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SelectGame extends Activity {
	public static final int MULTIPLAYER_PHONE = 1;
	//public static final int MULTIPLAYER_BLUETOOTH = 2;
	public static final int SINGLEPLAYER = 2;
	private int gameMode = MULTIPLAYER_PHONE;
	private ListView lv;
	private Spinner spinner;	
	
	public final static String SELECTED_GAME = "com.capricorn.fourline.SELECTED_GAME";
	public final static String SELECTED_GAMEMODE = "com.capricorn.fourline.SELECTED_GAMEMODE";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  setContentView(R.layout.activity_main);
	  String[] GAMES = getResources().getStringArray(R.array.games);
	  
	  spinner = (Spinner) findViewById(R.id.games_spinner);
	  ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
		        R.array.gamemodes, android.R.layout.simple_spinner_item);
	  adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	  spinner.setAdapter(adapter);
	  spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
		  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			  gameMode = position+1;
			  switch (gameMode) {
			  case SINGLEPLAYER:
				  lv.setAdapter(new ArrayAdapter<String>(SelectGame.this, R.layout.list_item2, getResources().getStringArray(R.array.singleplayergames)));
				  break;
			  case MULTIPLAYER_PHONE:
				  lv.setAdapter(new ArrayAdapter<String>(SelectGame.this, R.layout.list_item, getResources().getStringArray(R.array.games)));
				  break;
			  /*case MULTIPLAYER_BLUETOOTH:
				  lv.setAdapter(new ArrayAdapter<String>(SelectGame.this, R.layout.list_item3, getResources().getStringArray(R.array.bluegames)));
				  break;*/
			  }			
			  
		  }
		  public void onNothingSelected(AdapterView<?> parent) {
		  }
	  });
	  
	  lv = (ListView) findViewById(R.id.listview);
	  lv.setAdapter(new ArrayAdapter<String>(this, R.layout.list_item, GAMES));	  
	  lv.setTextFilterEnabled(true);
	  
	  lv.setOnItemClickListener(new OnItemClickListener() {
	    public void onItemClick(AdapterView<?> parent, View view,
	        int position, long id) {
	      // When clicked, show a toast with the TextView text
	      Toast.makeText(getApplicationContext(), /*((TextView) view).getText()*/Integer.toString(position),
	          Toast.LENGTH_SHORT).show();
	      Intent intent;
	      //if (gameMode==MULTIPLAYER_BLUETOOTH)
	      //	  intent = new Intent(SelectGame.this,BlueActivity.class);
	      //else
	    	  intent = new Intent(SelectGame.this,MainActivity.class);
	      intent.putExtra(SELECTED_GAME, position);
	      intent.putExtra(SELECTED_GAMEMODE, gameMode);
			startActivity(intent);
	    }
	  });
	  
	}

}