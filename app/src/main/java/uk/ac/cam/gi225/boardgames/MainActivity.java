package uk.ac.cam.gi225.boardgames;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

// TODO write controls help dialog
// TODO set up admob sdk, google play services
public class MainActivity extends Activity {
	
	private SG game;
	private static final int DIALOG_EXIT = 1;
	private static final int DIALOG_HELP = 2;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
		int sl = intent.getIntExtra(SelectGame.SELECTED_GAME, 0);
		int gm = intent.getIntExtra(SelectGame.SELECTED_GAMEMODE, SelectGame.MULTIPLAYER_PHONE);
        setTitle(getResources().getStringArray(R.array.games)[sl]);   
        if (gm==SelectGame.MULTIPLAYER_PHONE)
	        switch(sl) {
	        case 0:
	        	game = new FourLine(this);
	        	break;
	        case 1:
	        	game = new Checker(this);
	        	break;
	        case 2:
	        	game = new Mill(this);
	        	break;
	        case 3:
	        	game = new aGame(this);
	        	break;
	        default:
	        	game = new FourLine(this);
	        }
        else if (gm==SelectGame.SINGLEPLAYER)
        	switch (sl) {
        	case 0:
        		game = new aGame(this);
        		break;
        	case 1:
        		game = new FourLine(this);
        		break;
        	default:
        		game = new aGame(this);
        	}
        game.setGameMode(gm);
		setContentView(game);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.new_game:
                //newGame();
            	showDialog(DIALOG_EXIT);
                return true;
            case R.id.help:            	
                //showHelp();
            	showDialog(DIALOG_HELP);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    @Override
    public void onBackPressed() {
    	// TODO no need to confirm exit when the game is over
    	showDialog(DIALOG_EXIT);
    }
    
    
    
    protected Dialog onCreateDialog(int id) {
        Dialog dialog;
        switch(id) {
        case DIALOG_EXIT:
            // do the work to define the pause Dialog
        	AlertDialog.Builder builder = new AlertDialog.Builder(this);
        	builder.setMessage("Are you sure you want to exit?")
        	       .setCancelable(false)
        	       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	                MainActivity.this.finish();
        	           }
        	       })
        	       .setNegativeButton("No", new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	                dialog.cancel();
        	           }
        	       });
        	AlertDialog alert = builder.create();
        	dialog = alert;
            break;
        case DIALOG_HELP:
        	Dialog dialog2 = new Dialog(this);

        	dialog2.setContentView(R.layout.custom_dialog);
        	dialog2.setTitle("Custom Dialog");

        	TextView text = (TextView) dialog2.findViewById(R.id.text2);
        	text.setText("Hello, this is a custom dialog!");
        	ImageView image = (ImageView) dialog2.findViewById(R.id.image2);
        	image.setImageResource(R.drawable.ic_help);
        	dialog = dialog2;
        	break;
        default:
            dialog = null;
        }
        return dialog;
    }
    
    
    
}