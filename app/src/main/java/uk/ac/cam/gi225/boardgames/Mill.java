package uk.ac.cam.gi225.boardgames;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.view.MotionEvent;

public class Mill extends SG {
	enum field { EMPTY, BLACK, WHITE, NO };
	private field round,winner;
	private field[][] table = new field[7][7];
	private boolean pickc,end;
	private int bn,wn;
	private int l[] = new int[2];
	private int cheight,cwidth,gsize,bright,bleft;
	private Paint ptable,pcircle,pline,pwhite,pblack,pwinrect,pwintext;

	public Mill(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		pickc=false;
		end = false;
		winner = field.EMPTY;
		bn = 9;
		wn = 9;
		round = field.WHITE;
		for (int i=0; i<7; i++)
			for (int j=0; j<7; j++)
				table[i][j] = field.NO;
		for (int i=0; i<7; i++)
			if (i!=3) {
				table[i][i] = field.EMPTY;
				table[i][6-i] = field.EMPTY;
				table[3][i] = field.EMPTY;
				table[i][3] = field.EMPTY;
			}
		ptable = new Paint();
		pcircle = new Paint();
		pline = new Paint();
		pwhite = new Paint();
		pblack = new Paint();
		pwinrect = new Paint();
		pwintext = new Paint();
		ptable.setColor(Color.GREEN);
		pcircle.setColor(Color.LTGRAY);
		pcircle.setAntiAlias(true);
		pline.setColor(Color.BLACK);
		pline.setStrokeCap(Cap.ROUND);
		pline.setStyle(Style.FILL_AND_STROKE);
		pline.setStrokeWidth(3);
		pline.setStrokeJoin(Join.ROUND);
		pwhite.setColor(Color.WHITE);
		pwhite.setAntiAlias(true);
		pwhite.setTextAlign(Align.CENTER);
		pblack.setColor(Color.BLACK);
		pblack.setAntiAlias(true);
		pblack.setTextAlign(Align.CENTER);
		pwinrect.setColor(Color.LTGRAY);
		pwinrect.setAlpha(170);
		pwintext.setColor(Color.YELLOW);
		pwintext.setAlpha(170);
		pwintext.setTextAlign(Align.CENTER);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!end) {
			if ((event.getX(event.getPointerCount()-1)>=bleft)&&(event.getX(event.getPointerCount()-1)<=bright)&&(event.getY(event.getPointerCount()-1)<=gsize)) {
				int sx = 1,sy = 1;
				if (event.getAction()==MotionEvent.ACTION_DOWN) {
					for (int i=1; i<=7; i++)
						if ((event.getX()>=bleft+gsize/7*(i-1))&&(event.getX()<=bleft+gsize/7*i)) {
							sx = i;
							break;
						}
					for (int i=1; i<=7; i++)
						if ((event.getY()>=gsize/7*(i-1))&&(event.getY()<=gsize/7*i)) {
							sy = i;
							break;
						}
					l = new int[] {sx,sy};
				}
				if (event.getAction()==MotionEvent.ACTION_UP) {
					for (int i=1; i<=7; i++)
						if ((event.getX()>=bleft+gsize/7*(i-1))&&(event.getX()<=bleft+gsize/7*i)) {
							sx = i;
							break;
						}
					for (int i=1; i<=7; i++)
						if ((event.getY()>=gsize/7*(i-1))&&(event.getY()<=gsize/7*i)) {
							sy = i;
							break;
						}
					if ((sx==l[0])&&(sy==l[1]))
						putc(sx-1,sy-1);
					else if ((round==field.WHITE)?(wn==0):(bn==0))
						movec(sx-1,sy-1,l[0]-1,l[1]-1);			
				}
			}
		}
		return true;		
	}
	
	private void putc(int sx, int sy) {
		if (pickc&&(table[sx][sy]==((round==field.WHITE) ? field.BLACK : field.WHITE))) {
			boolean cp=true;
			cp = !ismill(new int[] {sx,sy},(round==field.WHITE) ? field.BLACK : field.WHITE);
			if (cp||onlymills((round==field.WHITE) ? field.BLACK : field.WHITE)) {
				pickc = false;
				table[sx][sy]=field.EMPTY;
				if (noleft((round==field.WHITE) ? field.BLACK : field.WHITE)) {
					winner = round;
					end = true;
				}
				round = (round==field.WHITE) ? field.BLACK : field.WHITE;
				invalidate();
			}
			return;
		}
		if ((table[sx][sy]==field.EMPTY)&&((round==field.WHITE)?(wn>0):(bn>0))) {
			table[sx][sy] = round;
			switch(round) {
			case WHITE: wn--; break;
			case BLACK: bn--; break;
			}
			round = (round==field.WHITE) ? field.BLACK : field.WHITE;
			invalidate();
		}
	}
	
	private void movec(int lx, int ly, int sx, int sy) {
		if ((table[lx][ly]==field.EMPTY)&&(table[sx][sy]==round)&&(!pickc))
			if (  ( ((sx==lx)&&((Math.abs(sy-ly)<=Math.abs(3-sx))))||((sx==3)&&(Math.abs(sy-ly)<=1)) ) ||
				  ( ((sy==ly)&&((Math.abs(sx-lx)<=Math.abs(3-sy))))||((sy==3)&&(Math.abs(sx-lx)<=1)) ) ||
				    threeleft(round)  )
			{
				table[lx][ly] = round;
				table[sx][sy] = field.EMPTY;
				pickc = ismill(new int[] {lx,ly},round);
				if (!pickc)
					round = (round==field.WHITE) ? field.BLACK : field.WHITE;
				invalidate();
			}
	}
	
	private boolean noleft(field p) {
		for (int i=0; i<7; i++)
			for (int j=0; j<7; j++)
				if (table[i][j]==p)
					return false;
		return true;
	}
	
	private boolean threeleft(field p) {
		int n = 0;
		for (int i=0; i<7; i++)
			for (int j=0; j<7; j++)
				if (table[i][j]==p)
					n++;
		if (n<=3)
			return true;
		return false;
	}
	
	private boolean onlymills(field p) {
		for (int i=0; i<7; i++)
			for (int j=0; j<7; j++)
				if ((table[i][j]==p)&&(!ismill(new int[] {i,j},p)))
					return false;
		return true;
	}
	
	private boolean ismill(int[] coords,field col) {
		if ((table[coords[0]][coords[0]]==table[coords[0]][3])&&(table[coords[0]][coords[0]]==table[coords[0]][6-coords[0]])&&(table[coords[0]][coords[0]]==col))
			return true;
		if ((table[coords[1]][coords[1]]==table[3][coords[1]])&&(table[coords[1]][coords[1]]==table[6-coords[1]][coords[1]])&&(table[coords[1]][coords[1]]==col))
			return true;
		if ((coords[0]==3)&&(coords[1]<3)&&(table[3][0]==table[3][1])&&(table[3][0]==table[3][2])&&(table[3][0]==col))
			return true;
		if ((coords[0]==3)&&(coords[1]>3)&&(table[3][4]==table[3][5])&&(table[3][4]==table[3][6])&&(table[3][4]==col))
			return true;
		if ((coords[1]==3)&&(coords[0]<3)&&(table[0][3]==table[1][3])&&(table[0][3]==table[2][3])&&(table[0][3]==col))
			return true;
		if ((coords[1]==3)&&(coords[0]>3)&&(table[4][3]==table[5][3])&&(table[4][3]==table[6][3])&&(table[4][3]==col))
			return true;
		return false;		
	}
	
	@Override
	public void onDraw(Canvas canvas) {
		cheight = canvas.getHeight();
		cwidth = canvas.getWidth();			
		gsize = cwidth < cheight ? cwidth : cheight;
		bleft = cwidth/2-gsize/2;
		bright = cwidth/2+gsize/2;
		pwintext.setTextSize(gsize/7);
		pblack.setTextSize(gsize/7*2);
		pwhite.setTextSize(gsize/7*2);
		canvas.drawRect(bleft, 0, bright, gsize, ptable);	
		for (int i=0; i<3; i++) {
			canvas.drawLine(bleft+gsize/14+i*gsize/7, gsize/14+i*gsize/7, bright-gsize/14-i*gsize/7, gsize/14+i*gsize/7, pline);
			canvas.drawLine(bleft+gsize/14+i*gsize/7, gsize-gsize/14-i*gsize/7, bright-gsize/14-i*gsize/7, gsize-gsize/14-i*gsize/7, pline);
			canvas.drawLine(bleft+gsize/14+i*gsize/7, gsize/14+i*gsize/7, bleft+gsize/14+i*gsize/7, gsize-gsize/14-i*gsize/7, pline);
			canvas.drawLine(bright-gsize/14-i*gsize/7, gsize/14+i*gsize/7, bright-gsize/14-i*gsize/7, gsize-gsize/14-i*gsize/7, pline);
		}
		canvas.drawLine(cwidth/2, gsize/14, cwidth/2, gsize/2-gsize/7, pline);
		canvas.drawLine(cwidth/2, gsize-gsize/14, cwidth/2, gsize/2+gsize/7, pline);
		canvas.drawLine(bleft+gsize/14, gsize/2, cwidth/2-gsize/7, gsize/2, pline);
		canvas.drawLine(bright-gsize/14, gsize/2, cwidth/2+gsize/7, gsize/2, pline);
		for (int i=0; i<7; i++)
			if (i!=3) {
				canvas.drawCircle(cwidth/2, gsize/14+i*gsize/7, gsize/28, pcircle);
				canvas.drawCircle(bleft+gsize/14+i*gsize/7, gsize/2, gsize/28, pcircle);
				canvas.drawCircle(bleft+gsize/14+i*gsize/7, gsize/14+i*gsize/7, gsize/28, pcircle);
				canvas.drawCircle(bright-gsize/14-i*gsize/7, gsize/14+i*gsize/7, gsize/28, pcircle);
			}
		for (int i=0; i<7; i++)
			for (int j=0; j<7; j++)
				switch(table[i][j]) {
				case WHITE:
					canvas.drawCircle(bleft+i*gsize/7+gsize/14, j*gsize/7+gsize/14, gsize/20, pwhite);
					break;
				case BLACK:
					canvas.drawCircle(bleft+i*gsize/7+gsize/14, j*gsize/7+gsize/14, gsize/20, pblack);
					break;
				}
		if (end) {
			canvas.drawRect(bleft, gsize/7*2, bright, gsize/7*6, pwinrect);
			canvas.drawText(getResources().getString(R.string.winner)+":", cwidth/2, gsize/7*3, pwintext);
			switch(winner) {
			case WHITE:
				canvas.drawText(getResources().getString(R.string.white), cwidth/2, gsize/7*5, pwhite);
				break;
			case BLACK:
				canvas.drawText(getResources().getString(R.string.black), cwidth/2, gsize/7*5, pblack);
				break;
			}			
		}
		//
	}

}
