package uk.ac.cam.gi225.boardgames;

import java.util.Random;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.view.KeyEvent;
import android.view.MotionEvent;

/**
 * four in a row/four in a line game
 * 
 * @author gabor
 *
 */
public class FourLine extends SG {
	private static final int CYAN = -1;
	private static final int GREEN = 1;
	private static final int EMPTY = 0;
	private static final int NONE = 0;
	private static final int DRAW = 2;
	private int[][] table = new int[7][6];
	private short[] gheight = new short[7];
	private boolean keys;
	private Paint p;
	private int cheight, cwidth, gsize, round, winner, me;
	private int[] line;
	private short kcol;


	public FourLine(Context context) {
		super(context);

		for (int i = 0; i < 7; i++)
			gheight[i] = 0;
		for (int i = 0; i < 7; i++)
			for (int j = 0; j < 6; j++)
				table[i][j] = EMPTY;
		line = new int[] { 0, 0, 0, 0 };
		round = GREEN;
		winner = NONE;
		keys = false;
		kcol = 1;
		p = new Paint();
		p.setTextAlign(Align.CENTER);
		p.setAntiAlias(true);
	}

	@Override
	public void setGameMode(int gm) {
		gameMode = gm;
		if (gameMode == SelectGame.SINGLEPLAYER) {
			Random r = new Random();
			me = r.nextBoolean() ? CYAN : GREEN;
			if (round != me) {
				AIMove();
				invalidate();
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (winner == NONE)
			if ((event.getX() >= cwidth / 2 - gsize / 2)
					&& (event.getX() <= cwidth / 2 + gsize / 2)
					&& (event.getY() <= gsize / 7)
					&& (((gameMode == SelectGame.SINGLEPLAYER) && (round == me)) || (gameMode == SelectGame.MULTIPLAYER_PHONE)))
				for (int i = 1; i <= 7; i++)
					if ((event.getX() >= cwidth / 2 - gsize / 2 + gsize / 7
							* (i - 1))
							&& (event.getX() <= cwidth / 2 - gsize / 2 + gsize
									/ 7 * i)) {
						putc(i - 1);
						if ((gameMode == SelectGame.SINGLEPLAYER)
								&& (winner == NONE)) {
							AIMove();
							if (round != me)
								round = me;
						}
						break;
					}
		return false;
	}

	private void putc(int a) {
		if (gheight[a] < 6) {
			gheight[a]++;
			table[a][6 - gheight[a]] = round;
			// horizontal
			int ah = 6 - gheight[a];
			for (int j = (a - 2 > 0) ? (a - 3) : 0; j < ((a <= 3) ? (a + 1) : 4); j++)
				if ((table[j][ah] == table[j + 1][ah])
						&& (table[j][ah] == table[j + 2][ah])
						&& (table[j][ah] == table[j + 3][ah])) {
					winner = round;
					line = new int[] { j, ah, j + 3, ah };
				}
			// vertical
			for (int j = (ah - 3 >= 0) ? (ah - 3) : 0; j <= ((ah < 3) ? ah : 2); j++)
				if ((table[a][j] == table[a][j + 1])
						&& (table[a][ah] == table[a][j + 2])
						&& (table[a][j] == table[a][j + 3])) {
					winner = round;
					line = new int[] { a, j, a, j + 3 };
				}
			// diagonal \
			int d1 = Math.min((a > 2) ? 3 : (a), (ah > 2) ? 3 : ah);
			int d2 = Math.min((a <= 3) ? 0 : (3 - a), (ah < 3) ? 0 : (2 - ah));
			for (int j = -d1; j <= d2; j++)
				if ((table[a + j][ah + j] == table[a + j + 1][ah + j + 1])
						&& (table[a + j][ah + j] == table[a + j + 2][ah + j + 2])
						&& (table[a + j][ah + j] == table[a + j + 3][ah + j + 3])) {
					winner = round;
					line = new int[] { a + j, ah + j, a + j + 3, ah + j + 3 };
				}
			// diagonal /
			for (int j = 0; j <= 3; j++)
				for (int k = 3; k < 6; k++)
					if ((table[j][k] == table[j + 1][k - 1])
							&& (table[j][k] == table[j + 2][k - 2])
							&& (table[j][k] == table[j + 3][k - 3])
							&& (table[j][k] == round)) {
						winner = round;
						line = new int[] { j, k, j + 3, k - 3 };
					}
			// check draw (table full)
			boolean d = true;
			for (int i = 0; i < 7; i++)
				for (int j = 0; j < 6; j++)
					if (table[i][j] == EMPTY) {
						d = false;
						break;
					}
			if (d)
				winner = DRAW;

			round *= -1;
			invalidate();
		}

	}

	// FIXME AI
	// FIXME AI doesn't notice three in a row
	private boolean AIMove() {
		if (AIWin())
			return true;
		if (AIBlock())
			return true;
		Random r = new Random();
		while (round != me)
			putc(r.nextInt(6));
		return false;
	}

	private boolean AIWin() {
		for (int i = 0; i < 7; i++)
			if ((gheight[i] < 6) && (table[i][5 - gheight[i]] == EMPTY)) {
				table[i][5 - gheight[i]] = round;
				if (AICheckWin(i, 5 - gheight[i])) {
					table[i][5 - gheight[i]] = EMPTY;
					putc(i);
					return true;
				}
				table[i][5 - gheight[i]] = EMPTY;
			}
		return false;
	}

	private boolean AIBlock() {
		for (int i = 0; i < 7; i++)
			if ((gheight[i] < 6) && (table[i][5 - gheight[i]] == EMPTY)) {
				table[i][5 - gheight[i]] = round * -1;
				if (AICheckWin(i, 5 - gheight[i])) {
					table[i][5 - gheight[i]] = EMPTY;
					putc(i);
					return true;
				}
				table[i][5 - gheight[i]] = EMPTY;
			}
		return false;
	}

	private boolean AICheckWin(int x, int y) {
		for (int j = (x - 2 > 0) ? (x - 3) : 0; j < ((x <= 3) ? (x + 1) : 4); j++)
			if ((table[j][y] == table[j + 1][y])
					&& (table[j][y] == table[j + 2][y])
					&& (table[j][y] == table[j + 3][y]))
				return true;
		// vertical
		for (int j = (y - 3 >= 0) ? (y - 3) : 0; j <= ((y < 3) ? y : 2); j++)
			if ((table[x][j] == table[x][j + 1])
					&& (table[x][y] == table[x][j + 2])
					&& (table[x][j] == table[x][j + 3]))
				return true;
		// diagonal \
		int d1 = Math.min((x > 2) ? 3 : (x), (y > 2) ? 3 : y);
		int d2 = Math.min((x <= 3) ? 0 : (3 - x), (y < 3) ? 0 : (2 - y));
		for (int j = -d1; j <= d2; j++)
			if ((table[x + j][y + j] == table[x + j + 1][y + j + 1])
					&& (table[x + j][y + j] == table[x + j + 2][y + j + 2])
					&& (table[x + j][y + j] == table[x + j + 3][y + j + 3]))
				return true;
		// diagonal /
		for (int j = 0; j <= 3; j++)
			for (int k = 3; k < 6; k++)
				if ((table[j][k] == table[j + 1][k - 1])
						&& (table[j][k] == table[j + 2][k - 2])
						&& (table[j][k] == table[j + 3][k - 3])
						&& (table[j][k] != EMPTY))
					return true;
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent msg) {
		keys = true;
		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			if (kcol < 7)
				kcol++;
			break;
		case KeyEvent.KEYCODE_DPAD_LEFT:
			if (kcol > 1)
				kcol--;
			break;
		case KeyEvent.KEYCODE_DPAD_DOWN:
			putc(kcol - 1);
			break;
		case KeyEvent.KEYCODE_1:
			kcol = 1;
			putc(kcol - 1);
			break;
		case KeyEvent.KEYCODE_2:
			kcol = 2;
			putc(kcol - 1);
			break;
		case KeyEvent.KEYCODE_3:
			kcol = 3;
			putc(kcol - 1);
			break;
		case KeyEvent.KEYCODE_4:
			kcol = 4;
			putc(kcol - 1);
			break;
		case KeyEvent.KEYCODE_5:
			kcol = 5;
			putc(kcol - 1);
			break;
		case KeyEvent.KEYCODE_6:
			kcol = 6;
			putc(kcol - 1);
			break;
		case KeyEvent.KEYCODE_7:
			kcol = 7;
			putc(kcol - 1);
			break;
		case KeyEvent.KEYCODE_ENTER:
			putc(kcol - 1);
			break;
		case KeyEvent.KEYCODE_DPAD_CENTER:
			putc(kcol - 1);
			break;
		}
		return super.onKeyDown(keyCode, msg);
	}

	@Override
	public void onDraw(Canvas canvas) {
		cheight = canvas.getHeight();
		cwidth = canvas.getWidth();
		// hardware acceleration does not support drawVertices
		gsize = cwidth < cheight ? cwidth : cheight;
		switch (round) {
		case CYAN:
			p.setColor(Color.CYAN);
			break;
		case GREEN:
			p.setColor(Color.GREEN);
		}
		p.setStrokeWidth(0);
		p.setAlpha(255);
		if (winner == NONE)
			for (int i = 1; i <= 7; i++)
				if (gheight[i - 1] < 6) {
					canvas.drawRect(cwidth / 2 - gsize / 2 + (i - 1) * gsize
							/ 7 + gsize / 28, 0, cwidth / 2 - gsize / 2 + i
							* gsize / 7 - gsize / 28, gsize / 14, p);
					for (int j = cwidth / 2 - gsize / 2 + (i - 1) * gsize / 7; j <= cwidth
							/ 2 - gsize / 2 + i * gsize / 7; j++)
						canvas.drawLine(
								j,
								gsize / 14,
								j,
								gsize
										/ 7
										- Math.abs(cwidth / 2 - gsize / 2
												+ (2 * i - 1) * gsize / 14 - j),
								p);
				}
		if (keys && (winner == NONE)) {
			// draw arrow outlines
			p.setColor(Color.RED);
			p.setStrokeWidth(2);
			canvas.drawLine(cwidth / 2 - gsize / 2 + (kcol - 1) * gsize / 7
					+ gsize / 28, 0, cwidth / 2 - gsize / 2 + (kcol - 1)
					* gsize / 7 + gsize / 28, gsize / 14, p);
			canvas.drawLine(cwidth / 2 - gsize / 2 + kcol * gsize / 7 - gsize
					/ 28, 0, cwidth / 2 - gsize / 2 + kcol * gsize / 7 - gsize
					/ 28, gsize / 14, p);
			canvas.drawLine(cwidth / 2 - gsize / 2 + (kcol - 1) * gsize / 7
					+ gsize / 28, gsize / 14, cwidth / 2 - gsize / 2
					+ (kcol - 1) * gsize / 7, gsize / 14, p);
			canvas.drawLine(cwidth / 2 - gsize / 2 + kcol * gsize / 7 - gsize
					/ 28, gsize / 14,
					cwidth / 2 - gsize / 2 + kcol * gsize / 7, gsize / 14, p);
			canvas.drawLine(cwidth / 2 - gsize / 2 + (kcol - 1) * gsize / 7,
					gsize / 14, cwidth / 2 - gsize / 2 + (kcol - 1) * gsize / 7
							+ gsize / 14, gsize / 7, p);
			canvas.drawLine(cwidth / 2 - gsize / 2 + kcol * gsize / 7,
					gsize / 14, cwidth / 2 - gsize / 2 + kcol * gsize / 7
							- gsize / 14, gsize / 7, p);
		}
		// yea I managed to draw a goddamn arrow after all
		p.setColor(Color.BLUE);
		p.setStrokeWidth(0);
		for (int i = 1; i <= 7; i++)
			for (int j = 1; j <= 6; j++)
				canvas.drawRect(cwidth / 2 - gsize / 2 + (i - 1) * gsize / 7, j
						* gsize / 7, cwidth / 2 - gsize / 2 + i * gsize / 7,
						(j + 1) * gsize / 7, p);
		p.setStrokeWidth(4);
		p.setColor(Color.MAGENTA);
		p.setAlpha(170);
		for (int i = 0; i <= 7; i++)
			canvas.drawLine(cwidth / 2 - gsize / 2 + i * gsize / 7, gsize / 7,
					cwidth / 2 - gsize / 2 + i * gsize / 7, gsize, p);
		for (int i = 1; i <= 7; i++)
			canvas.drawLine(cwidth / 2 - gsize / 2, i * gsize / 7, cwidth / 2
					+ gsize / 2, i * gsize / 7, p);
		p.setAlpha(255);
		p.setStrokeWidth(0);
		for (int i = 0; i < 7; i++)
			for (int j = 0; j < 6; j++) {
				switch (table[i][j]) {
				case EMPTY:
					p.setColor(Color.WHITE);
					break;
				case CYAN:
					p.setColor(Color.CYAN);
					break;
				case GREEN:
					p.setColor(Color.GREEN);
					break;
				default:
					canvas.drawText("ERROR!", cwidth / 2, cheight / 2, p);
				}
				canvas.drawCircle(cwidth / 2 - gsize / 2 + i * gsize / 7
						+ gsize / 14, (j + 1) * gsize / 7 + gsize / 14,
						gsize / 17, p);
			}
		if (winner != NONE) {
			p.setColor(Color.RED);
			p.setStrokeWidth(2);
			canvas.drawLine(cwidth / 2 - gsize / 2 + line[0] * gsize / 7
					+ gsize / 14, (line[1] + 1) * gsize / 7 + gsize / 14,
					cwidth / 2 - gsize / 2 + line[2] * gsize / 7 + gsize / 14,
					(line[3] + 1) * gsize / 7 + gsize / 14, p);
		}
	}
}