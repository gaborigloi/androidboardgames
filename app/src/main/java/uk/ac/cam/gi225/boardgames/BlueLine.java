package uk.ac.cam.gi225.boardgames;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;

// TODO fix this
public class BlueLine extends  View { 
	enum field { EMPTY, CYAN, GREEN };
	private field[][] table = new field[7][6];
	private short[] gheight = new short[7];
	private boolean keys;
	private field winner,round,mc;
	private Paint p;
	private int cheight, cwidth,gsize;
	private int[] line;
	private short kcol;
	private final Handler mHandler;	
	private static final int MODE_WAIT = 1;
	private static final int MODE_PLAY = 2;
	private static final int MODE_END = 3;
	private int gm;

	public BlueLine(Context context,Handler handler,int tc) {
		super(context);
		mHandler = handler;
		switch (tc) {
		case 1:
			mc = field.GREEN;
			break;
		case 2:
			mc = field.CYAN;
			break;
		}
		// TODO Auto-generated constructor stub
		for (int i=0; i<7; i++)
			gheight[i] = 0;
		for (int i=0; i<7; i++)
			for (int j=0; j<6; j++)
				table[i][j] = field.EMPTY;
		line = new int[] {0,0,0,0};
		round = field.GREEN;
		if (mc==round)
			gm = MODE_PLAY;
		else
			gm = MODE_WAIT;
		winner = field.EMPTY;
		keys = false;
		kcol = 1;
		p = new Paint();
		p.setTextAlign(Align.CENTER);
	}
	
	
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (gm==MODE_PLAY)
		if ((event.getX()>=cwidth/2-gsize/2)&&(event.getX()<=cwidth/2+gsize/2)&&(event.getY()<=gsize/7))
			for (int i=1; i<=7; i++)
				if ((event.getX()>=cwidth/2-gsize/2+gsize/7*(i-1))&&(event.getX()<=cwidth/2-gsize/2+gsize/7*i)) {
					putc(i-1);
					break;
				}
		return false;			
	}
	
	public void putc(int a) {
		if (gheight[a]<6) {
			if (round==mc) {
				mHandler.obtainMessage(BlueActivity.MESSAGE_WRITE, a, -1).sendToTarget();	
				gm = MODE_WAIT;
			} else {
				gm = MODE_PLAY;
			}
			gheight[a]++;
			table[a][6-gheight[a]] = round;
			// horizontal
			int ah = 6-gheight[a];
			for (int j=(a-2>0)?(a-3):0;j<((a<=3)?(a+1):4);j++)
				if ((table[j][ah]==table[j+1][ah])&&(table[j][ah]==table[j+2][ah])&&
						(table[j][ah]==table[j+3][ah])) {
					gm = MODE_END;
					winner = round;
					line = new int[] {j,ah,j+3,ah};
				}
			// vertical
			for (int j=(ah-3>=0)?(ah-3):0;j<=((ah<3)?ah:2);j++)
				if ((table[a][j]==table[a][j+1])&&(table[a][ah]==table[a][j+2])&&
						(table[a][j]==table[a][j+3])) {
					gm = MODE_END;
					winner = round;
					line = new int[] {a,j,a,j+3};
				}
			// diagonal \
			int d1 = Math.min((a>2)?3:(a), (ah>2)?3:ah);
			int d2 = Math.min((a<=3)?0:(3-a), (ah<3)?0:(2-ah));
			for (int j=-d1; j<=d2;j++)
				if ((table[a+j][ah+j]==table[a+j+1][ah+j+1])&&
						(table[a+j][ah+j]==table[a+j+2][ah+j+2])&&
						(table[a+j][ah+j]==table[a+j+3][ah+j+3])) {
					gm = MODE_END;
					winner = round;
					line = new int[] {a+j,ah+j,a+j+3,ah+j+3};
				}
			// diagonal /							
			for (int j=0; j<=3; j++)
				for (int k=3; k<6; k++)
					if ((table[j][k]==table[j+1][k-1])&&(table[j][k]==table[j+2][k-2])&&
							(table[j][k]==table[j+3][k-3])&&(table[j][k]==round)) {
						gm = MODE_END;
						winner = round;
						line = new int[] {j,k,j+3,k-3};
					}
			round = (round==field.GREEN) ? field.CYAN : field.GREEN;
			invalidate();
		}
		
	}
	
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent msg) {
		keys = true;
		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			if (kcol<7)
				kcol++;
			break;
		case KeyEvent.KEYCODE_DPAD_LEFT:
			if (kcol>1)
				kcol--;
			break;
		case KeyEvent.KEYCODE_DPAD_DOWN:
			putc(kcol-1);
			break;
		case KeyEvent.KEYCODE_1:
			kcol = 1;
			putc(kcol-1);
			break;
		case KeyEvent.KEYCODE_2:
			kcol = 2;
			putc(kcol-1);
			break;
		case KeyEvent.KEYCODE_3:
			kcol = 3;
			putc(kcol-1);
			break;
		case KeyEvent.KEYCODE_4:
			kcol = 4;
			putc(kcol-1);
			break;
		case KeyEvent.KEYCODE_5:
			kcol = 5;
			putc(kcol-1);
			break;
		case KeyEvent.KEYCODE_6:
			kcol = 6;
			putc(kcol-1);
			break;
		case KeyEvent.KEYCODE_7:
			kcol = 7;
			putc(kcol-1);
			break;
		case KeyEvent.KEYCODE_ENTER:
			putc(kcol-1);
			break;
		case KeyEvent.KEYCODE_DPAD_CENTER:
			putc(kcol-1);
			break;
		}
		return super.onKeyDown(keyCode, msg);
	}
	
	@Override
	public void onDraw(Canvas canvas) {
		cheight = canvas.getHeight();
		cwidth = canvas.getWidth();			
		// hardware acceleration does not support drawVertices
		gsize = cwidth < cheight ? cwidth : cheight;
		switch (round) {
		case CYAN:
			p.setColor(Color.CYAN);
			break;
		case GREEN:
			p.setColor(Color.GREEN);
		}
		p.setStrokeWidth(0);
		p.setAlpha(255);
		for (int i=1; i<=7; i++)
			if (gheight[i-1]<6) {
				canvas.drawRect(cwidth/2-gsize/2+(i-1)*gsize/7+gsize/28, 0, cwidth/2-gsize/2+i*gsize/7-gsize/28, gsize/14, p);
				for (int j=cwidth/2-gsize/2+(i-1)*gsize/7; j<=cwidth/2-gsize/2+i*gsize/7; j++)
					canvas.drawLine(j, gsize/14, j, gsize/7-Math.abs(cwidth/2-gsize/2+(2*i-1)*gsize/14-j), p);				
			}
		if (keys) {
			// draw arrow outlines
			p.setColor(Color.RED);
			p.setStrokeWidth(2);
			canvas.drawLine(cwidth/2-gsize/2+(kcol-1)*gsize/7+gsize/28, 0, 
					cwidth/2-gsize/2+(kcol-1)*gsize/7+gsize/28, gsize/14, p);
			canvas.drawLine(cwidth/2-gsize/2+kcol*gsize/7-gsize/28, 0, 
					cwidth/2-gsize/2+kcol*gsize/7-gsize/28, gsize/14, p);
			canvas.drawLine(cwidth/2-gsize/2+(kcol-1)*gsize/7+gsize/28, gsize/14,cwidth/2-gsize/2+(kcol-1)*gsize/7, gsize/14, p);
			canvas.drawLine(cwidth/2-gsize/2+kcol*gsize/7-gsize/28, gsize/14,cwidth/2-gsize/2+kcol*gsize/7, gsize/14, p);
			canvas.drawLine(cwidth/2-gsize/2+(kcol-1)*gsize/7, gsize/14,cwidth/2-gsize/2+(kcol-1)*gsize/7+gsize/14, gsize/7, p);
			canvas.drawLine(cwidth/2-gsize/2+kcol*gsize/7, gsize/14,cwidth/2-gsize/2+kcol*gsize/7-gsize/14, gsize/7, p);
		}
		// yea I managed to draw a goddamn arrow after all
		p.setColor(Color.BLUE);
		p.setStrokeWidth(0);
		for (int i=1; i<=7; i++)
			for (int j=1; j<=6; j++)
				canvas.drawRect(cwidth/2-gsize/2+(i-1)*gsize/7, j*gsize/7, cwidth/2-gsize/2+i*gsize/7, (j+1)*gsize/7, p);
		p.setStrokeWidth(4);
		p.setColor(Color.MAGENTA);
		p.setAlpha(170);
		for (int i=0; i<=7; i++)
			canvas.drawLine(cwidth/2-gsize/2+i*gsize/7, gsize/7, cwidth/2-gsize/2+i*gsize/7, gsize, p);
		for (int i=1; i<=7; i++)
			canvas.drawLine(cwidth/2-gsize/2, i*gsize/7, cwidth/2+gsize/2, i*gsize/7,p);
		p.setAlpha(255);
		p.setStrokeWidth(0);
		for (int i=0; i<7; i++)
			for (int j=0; j<6; j++) {
				switch (table[i][j]) {
				case EMPTY: 
					p.setColor(Color.WHITE);
					break;
				case CYAN:
					p.setColor(Color.CYAN);
					break;
				case GREEN:
					p.setColor(Color.GREEN);
					break;
				default:
					canvas.drawText("ERROR!", cwidth/2, cheight/2, p);
				}
				canvas.drawCircle(cwidth/2-gsize/2+i*gsize/7+gsize/14, (j+1)*gsize/7+gsize/14, gsize/17, p);
			}
		if (gm==MODE_END) {
			p.setColor(Color.RED);
			p.setStrokeWidth(2);
			canvas.drawLine(cwidth/2-gsize/2+line[0]*gsize/7+gsize/14, (line[1]+1)*gsize/7+gsize/14, cwidth/2-gsize/2+line[2]*gsize/7+gsize/14, (line[3]+1)*gsize/7+gsize/14, p);
			p.setColor(Color.GRAY);
			p.setStrokeWidth(0);
			p.setAlpha(170);
			canvas.drawRect(cwidth/2-gsize/2, gsize/7*2, cwidth/2+gsize/2, gsize/7*6, p);
			p.setTextSize(gsize/7);
			p.setColor(Color.YELLOW);
			p.setAlpha(170);
			canvas.drawText(getResources().getString(R.string.winner)+":", cwidth/2, gsize/7*3, p);
			p.setTextSize(gsize/7*2);
			switch(winner) {
			case GREEN:
				p.setColor(Color.GREEN);
				p.setAlpha(170);
				canvas.drawText(getResources().getString(R.string.green), cwidth/2, gsize/7*5, p);
				break;
			case CYAN:
				p.setColor(Color.CYAN);
				p.setAlpha(170);
				canvas.drawText(getResources().getString(R.string.cyan), cwidth/2, gsize/7*5, p);
				break;
			}
		}
		if (gm==MODE_WAIT) {
			p.setColor(Color.GRAY);
			p.setStrokeWidth(0);
			p.setAlpha(170);
			canvas.drawRect(cwidth/2-gsize/2, gsize/7*3, cwidth/2+gsize/2, gsize/7*5, p);
			p.setTextSize(gsize/7);
			p.setColor(Color.YELLOW);
			p.setAlpha(170);
			canvas.drawText(getResources().getString(R.string.waiting), cwidth/2, gsize/7*4, p);
		}
	}	
} 
