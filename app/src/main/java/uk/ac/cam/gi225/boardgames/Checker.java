package uk.ac.cam.gi225.boardgames;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.view.MotionEvent;

public class Checker extends SG {
	enum field { EMPTY, WHITE, BLACK, DWHITE, DBLACK };
	field[][] table = new field[8][8];
	private field winner,round;
	private Paint p;
	private int cheight,cwidth,gsize;
	private int[] sel;// = new int[2];
	private boolean end,isel,qkill;

	public Checker(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		for (int i=0; i<8; i++)
			for (int j=0; j<8; j++)
				table[i][j]= field.EMPTY;	
		for (int i=0; i<8; i++)
			for (int j=0; j<3; j++)
				if (i%2!=j%2)
					table[i][j] = field.WHITE;
		for (int i=0; i<8; i++)
			for (int j=5; j<8; j++)
				if (i%2!=j%2)
					table[i][j] = field.BLACK;
		round = field.WHITE;
		winner = field.EMPTY;
		end = false;
		isel = false;
		sel = new int[] {1,1};
		qkill = false;
		p = new Paint();
		p.setTextAlign(Align.CENTER);
		p.setAntiAlias(true);
	}
	
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!end) {
			int sx,sy;
			if ((event.getX()>=cwidth/2-gsize/2)&&(event.getX()<=cwidth/2+gsize/2)&&(event.getY()<=gsize)) {
				isel = true;
				sx = 1;
				sy = 1;
				for (int i=1; i<=8; i++)
					if ((event.getX()>=cwidth/2-gsize/2+gsize/8*(i-1))&&(event.getX()<=cwidth/2-gsize/2+gsize/8*i)) {
						sx = i;
						break;
					}
				for (int i=1; i<=8; i++)
					if ((event.getY()>=gsize/8*(i-1))&&(event.getY()<=gsize/8*i)) {
						sy = i;
						break;
					}
				pickd(sx-1,sy-1);
			}
		}
		return false;		
	}
	
	void pickd(int sx,  int sy) {
		if (own(sx,sy)) {
			if (!killer()||(tokill(sx,sy)&&killer())&&!qkill) {
			isel = true;
			sel = new int[] {sx,sy};
			invalidate();}
			return;
		}
		if (move(sx,sy)) {
			if (!killer()) {
				table[sx][sy] = table[sel[0]][sel[1]];
				table[sel[0]][sel[1]] = field.EMPTY;
				if ((round==field.WHITE)&&(sy==7))
					table[sx][sy] = field.DWHITE;
				if ((round==field.BLACK)&&(sy==0))
					table[sx][sy] = field.DBLACK;
				round=round==field.WHITE?field.BLACK:field.WHITE;
				isel = false;
				checkwin();
				invalidate();				
			}
			return;
		}
		if (kill(sx,sy)) {
			isel = false;
			table[sx][sy] = table[sel[0]][sel[1]];
			table[sel[0]][sel[1]] = field.EMPTY;
			table[(sx+sel[0])/2][(sy+sel[1])/2] = field.EMPTY;
			if ((round==field.WHITE)&&(sy==7))
				table[sx][sy] = field.DWHITE;
			if ((round==field.BLACK)&&(sy==0))
				table[sx][sy] = field.DBLACK;
			if (!(queen(sx,sy)&&tokill(sx,sy)) ) {
				qkill = false;
				round=round==field.WHITE?field.BLACK:field.WHITE;
			} else {
				isel = true;
				sel[0] = sx;
				sel[1] = sy;
				qkill = true;
			}
			checkwin();
			invalidate();
			return;
		}
	}
	
	private void checkwin() {
		// 0:- 1:white 2:black 3:none
		int b=0,w=0;
		for (int i=0; i<8; i++)
			for (int j=0; j<8; j++)
				if ((table[i][j]==field.BLACK)||(table[i][j]==field.DBLACK))
					b++;
				else if ((table[i][j]==field.WHITE)||(table[i][j]==field.DWHITE))
					w++;
		if (b==0) {
			winner = field.WHITE;
			end = true;
			return;
		}
		if (w==0) {
			winner = field.BLACK;
			end = true;	
			return;
		}
		if (!cmove(field.WHITE)) {
			winner = field.BLACK;
			end = true;
			return;
		}
		if (!cmove(field.BLACK)) {
			winner = field.WHITE;
			end = true;
			return;
		}
	}
	
	private boolean killer() {
		boolean ikill = false;
		for (int i=0; i<8; i++)
			for (int j=0; j<8; j++)
				if ((table[i][j]==round)||(table[i][j]==(round==field.WHITE?field.DWHITE:field.DBLACK)))
					if (tokill(i,j))
						ikill = true;
		return ikill;
	}	
	private boolean queen(int sx, int sy) {
		if ((table[sx][sy]==field.DBLACK)||(table[sx][sy]==field.DWHITE))
			return true;
		return false;
	}
	private boolean enemy(int sx, int sy) {
		if (table[sx][sy]==(round==field.BLACK?field.WHITE:field.BLACK))
			return true;
		if ((round==field.BLACK)&&(table[sx][sy]==field.DWHITE))
			return true;
		if ((round==field.WHITE)&&(table[sx][sy]==field.DBLACK))
			return true;
		return false;
	}
	private boolean move(int sx, int sy) {
		if (isel&&(round==field.WHITE)&&((sx==sel[0]+1)||(sx==sel[0]-1))&&(sy==sel[1]+1)&&(table[sx][sy]==field.EMPTY))
			return true;
		if (isel&&(round==field.BLACK)&&((sx==sel[0]+1)||(sx==sel[0]-1))&&(sy==sel[1]-1)&&(table[sx][sy]==field.EMPTY))
			return true;
		if (isel&&queen(sel[0],sel[1])&&((sx==sel[0]+1)||(sx==sel[0]-1))&&((sy==sel[1]+1)||(sy==sel[1]-1))&&(table[sx][sy]==field.EMPTY))
			return true;
		return false;
	}
	private boolean own(int sx, int sy) {
		if (table[sx][sy]==round)
			return true;
		if ((round==field.WHITE)&&(table[sx][sy]==field.DWHITE))
			return true;
		if ((round==field.BLACK)&&(table[sx][sy]==field.DBLACK))
			return true;
		return false;
	}
	private boolean kill(int sx, int sy) {
		if (isel&&(round==field.WHITE)&&(table[sx][sy]==field.EMPTY)&&((sx==sel[0]+2)||(sx==sel[0]-2))&&(sy==sel[1]+2)&&(enemy((sx+sel[0])/2,sy-1)))
			return true;
		if (isel&&(round==field.BLACK)&&(table[sx][sy]==field.EMPTY)&&((sx==sel[0]+2)||(sx==sel[0]-2))&&(sy==sel[1]-2)&&(enemy((sx+sel[0])/2,sy+1)))
			return true;
		if (isel&&queen(sel[0],sel[1])&&((sx==sel[0]-2)||(sx==sel[0]+2))&&((sy==sel[1]-2)||(sy==sel[1]+2))&&enemy( (sx+sel[0])/2,(sy+sel[1])/2 ) )
			return true;
		return false;
	}
	private boolean tokill(int sx, int sy) {
		if ( ((table[sx][sy]==field.WHITE)||(table[sx][sy]==field.DWHITE))&&(sx>1)&&(sy<6)&&(table[sx-2][sy+2]==field.EMPTY)&&((table[sx-1][sy+1]==field.BLACK)||(table[sx-1][sy+1]==field.DBLACK)))
			return true;
		if ( ((table[sx][sy]==field.WHITE)||(table[sx][sy]==field.DWHITE))&&(sx<6)&&(sy<6)&&(table[sx+2][sy+2]==field.EMPTY)&&((table[sx+1][sy+1]==field.BLACK)||(table[sx+1][sy+1]==field.DBLACK)))
			return true;
		if ( ((table[sx][sy]==field.BLACK)||(table[sx][sy]==field.DBLACK))&&(sx>1)&&(sy>1)&&(table[sx-2][sy-2]==field.EMPTY)&&((table[sx-1][sy-1]==field.WHITE)||(table[sx-1][sy-1]==field.DWHITE)))
			return true;
		if ( ((table[sx][sy]==field.BLACK)||(table[sx][sy]==field.DBLACK))&&(sx<6)&&(sy>1)&&(table[sx+2][sy-2]==field.EMPTY)&&((table[sx+1][sy-1]==field.WHITE)||(table[sx+1][sy-1]==field.DWHITE)))
			return true;		
		if ( (table[sx][sy]==field.DWHITE)&&(sx>1)&&(sy>1)&&(table[sx-2][sy-2]==field.EMPTY)&&((table[sx-1][sy-1]==field.BLACK)||(table[sx-1][sy-1]==field.DBLACK)))
			return true;
		if ( (table[sx][sy]==field.DWHITE)&&(sx<6)&&(sy>1)&&(table[sx+2][sy-2]==field.EMPTY)&&((table[sx+1][sy-1]==field.BLACK)||(table[sx+1][sy-1]==field.DBLACK)))
			return true;
		if ( (table[sx][sy]==field.DBLACK)&&(sx>1)&&(sy<6)&&(table[sx-2][sy+2]==field.EMPTY)&&((table[sx-1][sy+1]==field.WHITE)||(table[sx-1][sy+1]==field.DWHITE)))
			return true;
		if ( (table[sx][sy]==field.DBLACK)&&(sx<6)&&(sy<6)&&(table[sx+2][sy+2]==field.EMPTY)&&((table[sx+1][sy+1]==field.WHITE)||(table[sx+1][sy+1]==field.DWHITE)))
			return true;		
		return false;
	}
	private boolean cmove(field r) {
		for (int i=0; i<8; i++)
			for (int j=0; j<8; j++)
				if ((table[i][j]==r)||(table[i][j]==(r==field.WHITE?field.DWHITE:field.DBLACK)))
					if (tomove(i,j))
						return true;
		return false;
	}
	private boolean tomove(int sx, int sy) {
		if ( ((table[sx][sy]==field.WHITE)||(table[sx][sy]==field.DWHITE))&&(sx>0)&&(sy<7)&&(table[sx-1][sy+1]==field.EMPTY))
			return true;
		if ( ((table[sx][sy]==field.WHITE)||(table[sx][sy]==field.DWHITE))&&(sx<7)&&(sy<7)&&(table[sx+1][sy+1]==field.EMPTY))
			return true;
		if ( ((table[sx][sy]==field.BLACK)||(table[sx][sy]==field.DBLACK))&&(sx>0)&&(sy>0)&&(table[sx-1][sy-1]==field.EMPTY))
			return true;
		if ( ((table[sx][sy]==field.BLACK)||(table[sx][sy]==field.DBLACK))&&(sx<7)&&(sy>0)&&(table[sx+1][sy-1]==field.EMPTY))
			return true;		
		if ( (table[sx][sy]==field.DWHITE)&&(sx>0)&&(sy>0)&&(table[sx-1][sy-1]==field.EMPTY))
			return true;
		if ( (table[sx][sy]==field.DWHITE)&&(sx<7)&&(sy>0)&&(table[sx+1][sy-1]==field.EMPTY))
			return true;
		if ( (table[sx][sy]==field.DBLACK)&&(sx>0)&&(sy<7)&&(table[sx-1][sy+1]==field.EMPTY))
			return true;
		if ( (table[sx][sy]==field.DBLACK)&&(sx<7)&&(sy<7)&&(table[sx+1][sy+1]==field.EMPTY))
			return true;		
		return false;
	}
	

	
	@Override
	public void onDraw(Canvas canvas) {
		cheight = canvas.getHeight();
		cwidth = canvas.getWidth();
		gsize = cwidth < cheight ? cwidth : cheight;
		p.setStyle(Style.FILL);
		p.setAlpha(255);
		for (int i=1; i<=8; i++)
			for (int j=1; j<=8; j++) {
				if (i%2==j%2)
					p.setColor(Color.YELLOW);
				else
					p.setColor(Color.LTGRAY);
				canvas.drawRect(cwidth/2-gsize/2+(i-1)*gsize/8, (j-1)*gsize/8, cwidth/2-gsize/2+i*gsize/8,j*gsize/8, p);
			}
		if (!end) {
			p.setColor(Color.BLUE);
			p.setStyle(Style.STROKE);
			if (isel) {
				canvas.drawRect(cwidth/2-gsize/2+sel[0]*gsize/8, sel[1]*gsize/8, 
						cwidth/2-gsize/2+(sel[0]+1)*gsize/8, (sel[1]+1)*gsize/8, p);
			} else {
				boolean itkill = false;
				switch(round) {
				case WHITE:
					for (int i=0; i<8; i++)
						for (int j=0; j<8; j++)
							if ((table[i][j]==field.DWHITE)&&tokill(i,j)) {
								canvas.drawRect(cwidth/2-gsize/2+i*gsize/8, j*gsize/8, 
										cwidth/2-gsize/2+(i+1)*gsize/8, (j+1)*gsize/8, p);
								itkill = true;
							}
					break;
				case BLACK:
					for (int i=0; i<8; i++)
						for (int j=0; j<8; j++)
							if ((table[i][j]==field.DBLACK)&&tokill(i,j)) {
								canvas.drawRect(cwidth/2-gsize/2+i*gsize/8, j*gsize/8, 
										cwidth/2-gsize/2+(i+1)*gsize/8, (j+1)*gsize/8, p);
								itkill = true;
							}
				}
				for (int i=0; i<8; i++)
					for (int j=0; j<8; j++)
						if ((table[i][j]==round)&&tokill(i,j)) {
							canvas.drawRect(cwidth/2-gsize/2+i*gsize/8, j*gsize/8, 
									cwidth/2-gsize/2+(i+1)*gsize/8, (j+1)*gsize/8, p);
							itkill = true;
						}
				if (!itkill)
					for (int i=0; i<8; i++)
						for (int j=0; j<8; j++)
							if ((table[i][j]==round)||(table[i][j]==(round==field.WHITE?field.DWHITE:field.DBLACK)) )
								canvas.drawRect(cwidth/2-gsize/2+i*gsize/8, j*gsize/8, 
										cwidth/2-gsize/2+(i+1)*gsize/8, (j+1)*gsize/8, p);
			}
		}
		p.setStyle(Style.FILL);
		for (int i=0; i<8; i++)
			for (int j=0; j<8; j++) {
				switch (table[i][j]) {
				case WHITE:
					p.setColor(Color.WHITE);
					canvas.drawCircle(cwidth/2-gsize/2+i*gsize/8+gsize/16, j*gsize/8+gsize/16, gsize/16, p);
					break;
				case BLACK:
					p.setColor(Color.BLACK);
					canvas.drawCircle(cwidth/2-gsize/2+i*gsize/8+gsize/16, j*gsize/8+gsize/16, gsize/16, p);
					break;
				case DWHITE:
					p.setColor(Color.WHITE);
					canvas.drawCircle(cwidth/2-gsize/2+i*gsize/8+gsize/16, j*gsize/8+gsize/16, gsize/16, p);
					canvas.drawCircle(cwidth/2-gsize/2+i*gsize/8+gsize/16+gsize/32, j*gsize/8+gsize/16, gsize/16, p);
					for (int k= j*gsize/8; k<=(j+1)*gsize/8; k++)
						canvas.drawLine(cwidth/2-gsize/2+i*gsize/8+gsize/16, k, cwidth/2-gsize/2+i*gsize/8+gsize/16+gsize/32, k, p);
					break;
				case DBLACK:
					p.setColor(Color.BLACK);
					canvas.drawCircle(cwidth/2-gsize/2+i*gsize/8+gsize/16, j*gsize/8+gsize/16, gsize/16, p);
					canvas.drawCircle(cwidth/2-gsize/2+i*gsize/8+gsize/16+gsize/32, j*gsize/8+gsize/16, gsize/16, p);
					for (int k= j*gsize/8; k<=(j+1)*gsize/8; k++)
						canvas.drawLine(cwidth/2-gsize/2+i*gsize/8+gsize/16, k, cwidth/2-gsize/2+i*gsize/8+gsize/16+gsize/32, k, p);
					break;
				}
			}
		if (end) {
			p.setColor(Color.GRAY);
			p.setAlpha(170);
			canvas.drawRect(cwidth/2-gsize/2, gsize/8*2, cwidth/2+gsize/2, gsize/8*6, p);
			p.setTextSize(gsize/8);
			p.setColor(Color.YELLOW);
			p.setAlpha(170);
			canvas.drawText(getResources().getString(R.string.winner)+":", cwidth/2, gsize/8*3, p);
			p.setTextSize(gsize/8*2);
			switch(winner) {
			case WHITE:
				p.setColor(Color.WHITE);
				p.setAlpha(170);
				canvas.drawText(getResources().getString(R.string.white), cwidth/2, gsize/8*5, p);
				break;
			case BLACK:
				p.setColor(Color.BLACK);
				p.setAlpha(170);
				canvas.drawText(getResources().getString(R.string.black), cwidth/2, gsize/8*5, p);
				break;
			}
		}
		
	}

}
