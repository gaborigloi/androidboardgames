package uk.ac.cam.gi225.boardgames;

import android.content.Context;
import android.view.View;

public class SG extends View {
	protected int gameMode;

	public SG(Context context) {
		super(context);
		gameMode = SelectGame.MULTIPLAYER_PHONE;
	}
	
	public void setGameMode(int gm) {
		gameMode = gm;
	}

}
