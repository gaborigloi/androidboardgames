package uk.ac.cam.gi225.boardgames;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

// TODO fix this
public class BlueActivity extends Activity {
	
	private BluetoothAdapter mBluetoothAdapter = null;
	private static final int REQUEST_ENABLE_BT = 2;
	private static final int REQUEST_ENABLE_BT_DISC = 3;
	private static final String BT_SERVICE_NAME = "capricorn";
	private static final UUID MY_UUID = UUID.fromString("e1fb2ffa-42b0-4d4c-b1c9-7e53ef20a613");
	//e1fb2ffa-42b0-4d4c-b1c9-7e53ef20a613
	//capricorn
	private static final int GM_SELECT = 3;
	private static final int GM_HOST = 0;
	private static final int GM_JOIN = 1;
	public static final int MESSAGE_READ = 5;
	public static final int MESSAGE_WRITE = 6;
	private int gm;
	private ArrayAdapter<String> mArrayAdapter;
	private ListView lv;
	private ConnectedThread ct;
	private BlueLine bl;
	
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
            	    public void onReceive(Context context, Intent intent) {
            	        String action = intent.getAction();
            	        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
            	            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            	            mArrayAdapter.add(device.getName() + "\n" + device.getAddress());
            	        }
            	    }
            	};
            	
    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);	
		gm = GM_SELECT;
		findViewById(R.id.games_spinner).setVisibility(View.GONE);
		TextView tv = (TextView) findViewById(R.id.text);
		tv.setText(R.string.connect);
		lv = (ListView) findViewById(R.id.listview);
		mArrayAdapter = new ArrayAdapter<String> (this, R.layout.list_item);
		String[] BTP = getResources().getStringArray(R.array.bt_p);
		lv.setAdapter(new ArrayAdapter<String>(this, R.layout.list_item3, BTP));
		lv.setOnItemClickListener(new OnItemClickListener() {
		    public void onItemClick(AdapterView<?> parent, View view,
		        int position, long id) {
		      if (gm==GM_SELECT) {
			      Toast.makeText(getApplicationContext(), Integer.toString(position),
			          Toast.LENGTH_SHORT).show();
			      gm = position;
			      switch(position) {
			      case GM_HOST:
			    	  EnableBtDisc();
			    	  break;
			      case GM_JOIN:
			    	  lv.setAdapter(mArrayAdapter);
			    	  EnableBt();
			    	  break;		    	  
			      }
		      } else if (gm==GM_JOIN) {
		    	  String info = ((TextView) view).getText().toString();
		          String address = info.substring(info.length() - 17);
		          BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
		          ConnectThread ct = new ConnectThread(device);
		          ct.start();
		      }
		    }
		  });		
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter == null) {
		    Toast.makeText(this, getResources().getString(R.string.bt_not_available), Toast.LENGTH_LONG).show();
            finish();
            return;
		}		
	}	
	@Override
	public void onStart() {
		super.onStart();		
	}	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case REQUEST_ENABLE_BT:
            if (resultCode == Activity.RESULT_OK) {
                StartBt();
            } else {
                Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                finish();
            }
            break;
        case REQUEST_ENABLE_BT_DISC:
        	if (resultCode == Activity.RESULT_CANCELED) {
        		Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                finish();
        	} else {
        		AcceptThread at = new AcceptThread();
        		at.start();
        	}
        }        	
    }	
	void EnableBtDisc() {
		Intent discoverableIntent = new
				Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
				discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
		startActivityForResult(discoverableIntent,REQUEST_ENABLE_BT_DISC);				
	}	
	void EnableBt() {
		if (!mBluetoothAdapter.isEnabled()) {
		    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		} else {
			StartBt();
		}		
	}	
	void StartBt() {
		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
    	if (pairedDevices.size() > 0) {
    	    for (BluetoothDevice device : pairedDevices) {
    	        mArrayAdapter.add(device.getName() + "\n" + device.getAddress());
    	    }
    	}
    	mBluetoothAdapter.startDiscovery();    	
    	IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
    	registerReceiver(mReceiver, filter);
	}	
	private class AcceptThread extends Thread {
	    private final BluetoothServerSocket mmServerSocket;	 
	    public AcceptThread() {
	        BluetoothServerSocket tmp = null;
	        try {
	            tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(BT_SERVICE_NAME, MY_UUID);
	        } catch (IOException e) { }
	        mmServerSocket = tmp;
	    }	 
	    public void run() {
	        BluetoothSocket socket = null;
	        while (true) {
	            try {
	                socket = mmServerSocket.accept();
	            } catch (IOException e) {
	                break;
	            }
	            if (socket != null) {
	                manageConnectedSocket(socket);
	                try {
						mmServerSocket.close();
					} catch (IOException e) { }
	                break;
	            }
	        }
	    }	 
	    public void cancel() {
	        try {
	            mmServerSocket.close();
	        } catch (IOException e) { }
	    }
	}	
	void manageConnectedSocket(BluetoothSocket socket) {
		ct = new ConnectedThread(socket);
		ct.start();
		switch(gm) {
		case GM_HOST:
			bl = new BlueLine(this,mHandler,1);
			break;
		case GM_JOIN:
			bl = new BlueLine(this,mHandler,2);
			break;
		}
		setContentView(bl);
	}	
	private class ConnectThread extends Thread {
	    private final BluetoothSocket mmSocket;
	    private final BluetoothDevice mmDevice;	 
	    public ConnectThread(BluetoothDevice device) {
	        BluetoothSocket tmp = null;
	        mmDevice = device;	 
	        try {
	            tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
	        } catch (IOException e) { }
	        mmSocket = tmp;
	    }	 
	    public void run() {
	        mBluetoothAdapter.cancelDiscovery();	 
	        try {
	            mmSocket.connect();
	        } catch (IOException connectException) {
	            try {
	                mmSocket.close();
	            } catch (IOException closeException) { }
	            return;
	        }
	        manageConnectedSocket(mmSocket);
	    }	 
	    public void cancel() {
	        try {
	            mmSocket.close();
	        } catch (IOException e) { }
	    }
	}	
	private class ConnectedThread extends Thread {
	    private final BluetoothSocket mmSocket;
	    private final InputStream mmInStream;
	    private final OutputStream mmOutStream;	 
	    public ConnectedThread(BluetoothSocket socket) {
	        mmSocket = socket;
	        InputStream tmpIn = null;
	        OutputStream tmpOut = null;	 
	        try {
	            tmpIn = socket.getInputStream();
	            tmpOut = socket.getOutputStream();
	        } catch (IOException e) { }	 
	        mmInStream = tmpIn;
	        mmOutStream = tmpOut;
	    }	 
	    public void run() {
	        byte[] buffer = new byte[1024];
	        int bytes;	 
	        while (true) {
	            try {
	                bytes = mmInStream.read(buffer);
	                mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
	                        .sendToTarget();
	            } catch (IOException e) {
	                break;
	            }
	        }
	    }	 
	    public void write(byte[] bytes) {
	        try {
	            mmOutStream.write(bytes);
	        } catch (IOException e) { }
	    }	 
	    public void cancel() {
	        try {
	            mmSocket.close();
	        } catch (IOException e) { }
	    }
	}	
	private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_READ:     
            	byte[] readBuf = (byte[]) msg.obj;
            	if (bl!=null)
            		bl.putc(readBuf[1]);
                break;
            case MESSAGE_WRITE:
            	byte writeBuf[] = new byte[] {(byte) msg.arg1};
            	if (ct!=null)
            		ct.write(writeBuf);
            	break;
            }
        }
    };
}
